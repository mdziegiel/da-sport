package models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Exercise: A "fact", like "A Member does this, or that in a Facility, under the supervision of a Trainer",
 * since it's a kind of Event, there's also: ... " at this or that time."
 */

@Entity
@Table(name = "exercises")
public class Exercise extends Event
{
	@ManyToOne
	private Facility facility = new Facility();	// ...where the exercise takes place
	
	@ManyToOne
	private Member member = new Member(); 		// the participant
	
	@ManyToOne
	private Group group = new Group();		// the participants (either one of group - member, is null, depending on the kind of exercise (individual / group))
	
	@ManyToOne
	private Trainer trainer = new Trainer();	// the trainer, leader of the exercise
	
	@ManyToOne
	private Competition competition = new Competition(); // that the exercise belongs to
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Exercise> find = new Finder<>(Integer.class, Exercise.class);
	
	public Facility getFacility()		{ return facility; }
	public Member getMember()			{ return member; }
	public Group getGroup()				{ return group; }
	public Trainer getTrainer()			{ return trainer; }
	public Competition getCompetition()	{ return competition; }
	
	public void setFacility(Facility facility)	{ this.facility = facility; }
	public void setTrainer(Trainer trainer)		{ this.trainer = trainer; }
	public void setCompetition(Competition c)	{ competition = c; }
	
	public void setMember(Member member)
	{
		this.member = member;
		group = null;
	}
	
	public void setGroup(Group group)
	{
		this.group = group;
		member = null;
	}
	
	public boolean isGroup()
	{
		if (member == null) return true;
		return false;
	}
	
	public boolean isIndividual()
	{
		if (group == null) return true;
		return false;
	}
}