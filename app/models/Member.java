package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Member, a client of the company.
 */

@Entity
@Table(name = "members")
public class Member extends User
{
	private static final String roleName = "Member";	// a title, like "Member John Doe", for profile page
	
	@OneToMany(mappedBy = "member", cascade = CascadeType.PERSIST)
	private List<Exercise> exercises = new ArrayList<>();					// ... that the member has planned (schedule)
	
	@OneToMany(mappedBy = "participant", cascade = CascadeType.PERSIST)
	private List<Participation> participations = new ArrayList<>();			// ... in competitions
	
	@ManyToMany(mappedBy = "members", cascade = CascadeType.PERSIST)
	private List<Group> groups = new ArrayList<>();							// ... that the member belongs to
	
	@NotNull
	private int points = 0;									// account balance
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Member> find = new Finder<>(Integer.class, Member.class);
	
	public String getRoleName()						{ return roleName; }
	public List<Exercise> getExercises()			{ return exercises; }
	public List<Group> getGroups()					{ return groups; }
	public int getPoints()							{ return points; }
	public List<Participation> getParticipations()	{ return participations; }
	
	public void setExercises(List<Exercise> exercises)		{ this.exercises = exercises; }
	public void setGroups(List<Group> groups)				{ this.groups = groups; }
	public void setPoints(int points)						{ this.points = points; }
	public void setParticipations(List<Participation> p)	{ this.participations = p; }
}