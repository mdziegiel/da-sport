package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Basically an exercise (or exercises), but with rewards and winners. And it doesn't make sense to have a competition with one participant
 */

@Entity
@Table(name = "competitions")
public class Competition extends Event
{
	@ManyToOne
	private Trainer trainer = new Trainer();						// ... who added and supervises the competition
	
	private String rewards;							// description of rewards
	private int points;								// reward: it is possible to give points (the same points that one can buy) for winning the competition, otherwise this is 0
	
	@OneToMany(mappedBy = "competition", cascade = CascadeType.PERSIST)
	private List<Participation> participants = new ArrayList<>();	// list of pairs: member - points, member - points, ...
	
	@OneToMany(mappedBy = "competition", cascade = CascadeType.PERSIST)
	private List<Exercise> exercises = new ArrayList<>();			// a competition can be made up of more than one exercise
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Competition> find = new Finder<>(Integer.class, Competition.class);
	
	public Trainer getTrainer()						{ return trainer; }
	public String getRewards()						{ return rewards; }
	public int getPoints()							{ return points; }
	public List<Participation> getParticipants()	{ return participants; }
	public List<Exercise> getExercises()			{ return exercises; }
	
	public void setTrainer(Trainer trainer)							{ this.trainer = trainer; }
	public void setRewards(String rewards)							{ this.rewards = rewards; }
	public void setPoints(int points)								{ this.points = points; }
	public void setParticipants(List<Participation> participants)	{ this.participants = participants; }
	public void setExercises(List<Exercise> exercises)				{ this.exercises = exercises; }
}