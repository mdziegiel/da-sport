package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Trainer.
 */

@Entity
@Table(name = "trainers")
public class Trainer extends Employee
{
	private static final String roleName = "Trainer";	// a title, like "Trainer John Doe", for profile page
	
	@OneToMany(mappedBy = "trainer", cascade = CascadeType.PERSIST)
	private List<Exercise> schedule = new ArrayList<>();
	
	@OneToMany(mappedBy = "caretaker", cascade = CascadeType.PERSIST)
	private List<Facility> facilities = new ArrayList<>();					// ... that the Trainer takes care of
	
	@ManyToMany
	private List<Discipline> specialties = new ArrayList<>();
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Trainer> find = new Finder<>(Integer.class, Trainer.class);
	
	public String getRoleName()					{ return roleName; }
	public List<Exercise> getSchedule()			{ return schedule; }
	public List<Facility> getFacilities()		{ return facilities; }
	public List<Discipline> getSpecialties()	{ return specialties; }
	
	public void setSchedule(List<Exercise> schedule)			{ this.schedule = schedule; }
	public void setFacilities(List<Facility> facilities)		{ this.facilities = facilities; }
	public void setSpecialties(List<Discipline> specialties)	{ this.specialties = specialties; }
}