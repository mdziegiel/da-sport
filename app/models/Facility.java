package models;

// TODO import ...Image;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/** 
 * A facility is essentially a part of a club, a place with some equipment, allowing for doing exercises.
 */

@Entity
@Table(name = "facilities")
public class Facility extends Model
{
	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	private Club club = new Club();							// ... where the facility belongs
	
	@Required
	private String description;					// "this is a gym, where you can lift weights, this gym is awesome"
	//private Image image;						// a photo of the facility, or maybe a mono-chromatic icon, up to the client
	
	@ManyToOne
	private Trainer caretaker = new Trainer();					// ... who is responsible for the facility
	
	@Required
	private int slots;							// number of people who can use the facility at the same time, 0 if infinity
	
	@Required
	private int price;							// "points" per hour
	
	@OneToMany(mappedBy = "facility", cascade = CascadeType.PERSIST)
	private List<Exercise> exercises = new ArrayList<>();			// that take place in/on/at the facility
	
	@ManyToOne
	private Discount discount = new Discount();
	
	@ManyToMany
	private List<Discipline> disciplines = new ArrayList<>();	// ... that one can practice in this facility
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Facility> find = new Finder<>(Integer.class, Facility.class);
	
	public int getID()							{ return id; }
	public Club getClub()						{ return club; }
	public String getDescription()				{ return description; }
	//public Image getImage()					{ return image; }
	public Trainer getCaretaker()				{ return caretaker; }
	public int getSlots()						{ return slots; }
	public int getPrice()						{ return price; }
	public Discount getDiscount()				{ return discount; }
	public List<Discipline> getDisciplines()	{ return disciplines; }
	public List<Exercise> getExercises()		{ return exercises; }
	
	public void setID(int id)									{ this.id = id; }
	public void setClub(Club club)								{ this.club = club; }
	public void setDescription(String description)				{ this.description = description; }
	//public void setImage(Image image)							{ this.image = image; }
	public void setCaretaker(Trainer caretaker)					{ this.caretaker = caretaker; }
	public void setSlots(int slots)								{ this.slots = slots; }
	public void setPrice(int price)								{ this.price = price; }
	public void setDiscount(Discount discount)					{ this.discount = discount; }
	public void setDisciplines(List<Discipline> disciplines)	{ this.disciplines = disciplines; }
	public void setExercises(List<Exercise> exercises)			{ this.exercises = exercises; }
}