package models;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Administrator (a global one, admin of the system, rather than a club - that's a manager).
 */

@Entity
@Table(name = "administrators")
public class Administrator extends User
{
	private static final String roleName = "Administrator";	// a title, like "Administrator John Doe", for profile page
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Administrator> find = new Finder<>(Integer.class, Administrator.class);

	public String getRoleName() { return roleName; }
}