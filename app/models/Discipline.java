package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * Discipline.
 */

@Entity
@Table(name = "disciplines")
public class Discipline extends Model
{
	@Id
	@GeneratedValue
	private int id;
	
	@Required
	private String name;	// running, jumping, weight lifting, swimming...
	
	@ManyToMany(mappedBy = "specialties", cascade = CascadeType.PERSIST)
	private List<Trainer> trainers = new ArrayList<>();
	
	@ManyToMany(mappedBy = "disciplines", cascade = CascadeType.PERSIST)
	private List<Facility> facilities = new ArrayList<>();
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Discipline> find = new Finder<>(Integer.class, Discipline.class);
	
	public int getID()						{ return id; }
	public String getName()					{ return name; }
	public List<Trainer> getTrainers()		{ return trainers; }
	public List<Facility> getFacilities()	{ return facilities; }
	
	public void setID(int id)						{ this.id = id; }
	public void setName(String name)				{ this.name = name; }
	public void setTrainers(List<Trainer> trainers)	{ this.trainers = trainers; }
	public void setFacilities(List<Facility> f)		{ facilities = f; }
}