package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * A group is essentially a many-to-many entity denoting, that its members (want to) participate in some group exercises, such as any team sport match.
 * Or it might just be a group of friends.
 */

@Entity
@Table(name = "groups")
public class Group extends Model
{
	@Id
	@GeneratedValue
	private int id;
	
	@Required
	private String name;
	private String description;
	
	@ManyToMany
	private List<Member> members = new ArrayList<>();
	
	@ManyToOne
	private Member owner = new Member();
	
	@OneToMany(mappedBy = "group", cascade = CascadeType.PERSIST)
	private List<Exercise> exercises = new ArrayList<>();
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Group> find = new Finder<>(Integer.class, Group.class);
	
	public int getID()						{ return id; }
	public String getName()					{ return name; }
	public String getDescription()			{ return description; }
	public List<Member> getMembers()		{ return members; }
	public Member getOwner()				{ return owner; }
	public List<Exercise> getExercises()	{ return exercises; }
	
	public void setID(int id)								{ this.id = id; }
	public void setName(String name)						{ this.name = name; }
	public void setDescription(String description)			{ this.description = description; }
	public void setMembers(List<Member> members)			{ this.members = members; }
	public void setOwner(Member owner)						{ this.owner = owner; }
	public void setExercises(List<Exercise> exercises)		{ this.exercises = exercises; }
}