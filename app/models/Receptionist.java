package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Receptionist.
 */

@Entity
@Table(name = "receptionists")
public class Receptionist extends Employee
{
	@OneToMany(mappedBy = "receptionist", cascade = CascadeType.PERSIST)
	private List<Discount> discounts = new ArrayList<>();
	
	private static final String roleName = "Receptionist";	// a title, like "Receptionist John Doe", for profile page
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Receptionist> find = new Finder<>(Integer.class, Receptionist.class);

	public String getRoleName()				{ return roleName; }
	public List<Discount> getDiscounts()	{ return discounts; }
	
	public void setDiscounts(List<Discount> d)	{ discounts = d; }
}