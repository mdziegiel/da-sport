package models;

import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

/**
 * Someone working in one of the clubs, a Receptionist or Trainer (this is the base class for those).
 */

@MappedSuperclass
public abstract class Employee extends User
{
	@NotNull
	protected boolean manager = false;	// "this employee is the manager of the club, where they work"
	
	@ManyToOne
	protected Club club = new Club();	// ... where the employee works
	
	public static final long serialVersionUID = 02062015L;
	
	public boolean isManager()	{ return manager; }
	public Club getClub()		{ return club; }
	
	public void setManager(boolean manager)	{ this.manager = manager; }
	public void setClub(Club club)			{ this.club = club; }
}