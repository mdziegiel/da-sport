package models;

// TODO import ...Image;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * Base for Administrator, Member and Employee.
 */

@MappedSuperclass
public abstract class User extends Model
{
	@Id
	@GeneratedValue
	protected int id;
	
	protected String name;
	
	protected String surname;
	
	@Required
	protected String email;
	
	protected String description;
	
	//protected Image image;
	protected String passhash;
	protected boolean loggedIn,
	present;						// ... physically, in one of the clubs
	public static final long serialVersionUID = 14052015L;
	
	public int getID()				{ return id; }
	public String getName()			{ return name; }
	public String getSurname()		{ return surname; }
	public String getEmail()		{ return email; }
	public String getDescription()	{ return description; }
	//public Image getImage()		{ return image; }
	public boolean passwordCorrect(String password) throws NoSuchAlgorithmException
	{
		return passhash.equals(hash(password));
	}
	public boolean isLoggedIn()		{ return loggedIn; }
	public boolean isPresent()		{ return present; }
	
	public void setID(int id)						{ this.id = id; }
	public void setName(String name)				{ this.name = name; }
	public void setSurname(String surname)			{ this.surname = surname; }
	public void setEmail(String email)				{ this.email = email; }
	public void setDescription(String description)	{ this.description = description; }
	//public void setImage(Image image)				{ this.image = image; }
	public void setPassword(String password) throws NoSuchAlgorithmException
	{
		passhash = hash(password);
	}
	public void setLoggedIn(boolean loggedIn)		{ this.loggedIn = loggedIn; }
	public void setPresent(boolean present)			{ this.present = present; }
	
	public String hash(String input) throws NoSuchAlgorithmException
	{
		MessageDigest mDigest = MessageDigest.getInstance("SHA1");
        byte[] result = mDigest.digest(input.getBytes());
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < result.length; i++) sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
        return sb.toString();
	}
}