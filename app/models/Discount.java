package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Discount.
 */

@Entity
@Table(name = "discounts")
public class Discount extends Event
{
	@ManyToOne
	private Receptionist receptionist = new Receptionist();	// ... who added the discount
	
	@OneToMany(mappedBy = "discount", cascade = CascadeType.PERSIST)
	private List<Facility> facilities = new ArrayList<>();	// ... for which the discount applies
	
	private int points;					// ... less, so 20 here means price = basePrice - 20;
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Discount> find = new Finder<>(Integer.class, Discount.class);
	
	public Receptionist getReceptionist()	{ return receptionist; }
	public List<Facility> getFacilities()	{ return facilities; }
	public int getPoints()					{ return points; }
	
	public void setReceptionist(Receptionist receptionist)	{ this.receptionist = receptionist; }
	public void setPoints(int points)						{ this.points = points; }
	public void setFacilities(List<Facility> facilities)	{ this.facilities = facilities; }
}