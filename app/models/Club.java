package models;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
// TODO import ...Image;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * A club is one "unit" of the whole company, probably a building, or a floor in a building, with whatever is related, like equipment, staff.
 */

@Entity
@Table(name = "clubs")
public class Club extends Model
{
	@Id
	@GeneratedValue
	private int id;
	
	@OneToMany(mappedBy = "club", cascade = CascadeType.PERSIST)
	private List<Receptionist> receptionists = new ArrayList<>();	// ... who work in the club
	
	@OneToMany(mappedBy = "club", cascade = CascadeType.PERSIST)
	private List<Trainer> trainers = new ArrayList<>();				// ... who work in the club
	
	@OneToMany(mappedBy = "club", cascade = CascadeType.PERSIST)
	private List<Facility> facilities = new ArrayList<>();			// ... that the club has to offer
	
	@Required
	private String city, street, building,
	description;							// "this is a great club, come, use this club!"
	//private Image image;					// a photo of the club, or maybe a mono-chromatic icon, up to the client

	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Club> find = new Finder<>(Integer.class, Club.class);
	
	public int getID()								{ return id; }
	public List<Receptionist> getReceptionists()	{ return receptionists; }
	public List<Trainer> getTrainers()				{ return trainers; }
	public List<Facility> getFacilities()			{ return facilities; }
	public String getCity()							{ return city; }
	public String getStreet()						{ return street; }
	public String getBuilding()						{ return building; }
	public String getDescription()					{ return description; }
	//public Image getImage()						{ return image; }
	
	public void setID(int id)							{ this.id = id; }
	public void setReceptionists(List<Receptionist> r)	{ receptionists = r; }
	public void setTrainers(List<Trainer> t)			{ trainers = t; }
	public void setFacilities(List<Facility> f)			{ facilities = f; }
	public void setCity(String city)					{ this.city = city; }
	public void setStreet(String street)				{ this.street = street; }
	public void setBuilding(String building)			{ this.building = building; }
	public void setDescription(String description)		{ this.description = description; }
	//public void setImage(Image image)					{ this.image = image; }
}