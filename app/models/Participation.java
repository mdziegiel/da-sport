package models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import play.db.ebean.Model;

/**
 * Denotes the participation of a Member in a Competition, with a number of points.
 */

@Entity
@Table(name = "participations")
public class Participation extends Model
{
	@Id
	@GeneratedValue
	private int id;
	
	@ManyToOne
	private Competition competition = new Competition();
	
	@ManyToOne
	private Member participant = new Member();
	
	@NotNull
	private int points = 0;
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Participation> find = new Finder<>(Integer.class, Participation.class);
	
	public int getID()					{ return id; }
	public Competition getCompetition()	{ return competition; }
	public Member getParticipant()		{ return participant; }
	public int getPoints()				{ return points; }
	
	public void setID(int id)							{ this.id = id; }
	public void setCompetition(Competition competition)	{ this.competition = competition; }
	public void setParticipant(Member participant)		{ this.participant = participant; }
	public void setPoints(int points)					{ this.points = points; }
}
