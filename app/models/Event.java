package models;

import org.joda.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import play.data.validation.Constraints.Required;
import play.db.ebean.Model;

/**
 * Base for Exercise, Competition and Discount, but also, simply, an event for the news section on the home page.
 */

@Entity
@Table(name = "events")
public class Event extends Model
{
	@Id
	@GeneratedValue
	protected int id;
	
	protected LocalDateTime startTime;
	
	protected LocalDateTime endTime;
	
	@Required
	protected String title;
	
	protected String description;
	
	public static final long serialVersionUID = 02062015L;
	public static Finder<Integer, Event> find = new Finder<>(Integer.class, Event.class);
	
	public int getID()					{ return id; }
	public LocalDateTime getStartTime()	{ return startTime; }
	public LocalDateTime getEndTime()	{ return endTime; }
	public String getTitle()			{ return title; }
	public String getDescription()		{ return description; }
	
	public void setID(int id)							{ this.id = id; }
	public void setStartTime(LocalDateTime startTime)	{ this.startTime = startTime; }
	public void setEndTime(LocalDateTime endTime)		{ this.endTime = endTime; }
	public void setTitle(String title)					{ this.title = title; }
	public void setDescription(String description)		{ this.description = description; }
}