package controllers;

import org.joda.time.LocalDateTime;
//import java.time.LocalDateTime;
import models.Discipline;
import models.Discount;
import models.Member;

public class ReceptionistController
{
	void addSubscription(Member member, int points)
	{
		member.setPoints(member.getPoints() + points);
	}
	
	void addDiscount(Discipline discipline, int points, LocalDateTime timeout)
	{
		Discount discount = new Discount();
		discount.setPoints(points);
		discount.setEndTime(timeout);
		discount.save();
	}
	
	void removeDiscount(Discount discount)
	{
		discount.delete();
		discount = null;
	}
}
