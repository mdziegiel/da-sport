package controllers;

import models.Club;
import models.Employee;
import models.User;

public class AdministratorController
{
	void addClub()
	{
		Club club = new Club();
		//TODO
		club.save();
	}
	
	void removeClub(Club club )
	{
		//TODO
		club.delete();
		club = null;
	}
	
	void grantManager(Employee employee)
	{
		employee.setManager(true);
		//foreach other employee in their facility ensure manager is false TODO
	}
	
	void revokeManager(Employee employee)
	{
		employee.setManager(false);
	}
	
	void removeUser(User user)
	{
		//TODO
		user.delete();
		user = null;
	}
}
