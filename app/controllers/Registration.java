package controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import models.Member;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.register;

public class Registration extends Controller
{
	public static Result showRegisterForm()
	{
		return ok(register.render(null, null, null, null, null));
	}
	
	public static Result createMember()
	{
		final Map<String, String[]> values = request().body().asFormUrlEncoded();
		
		Member toSave = new Member();
		toSave.setName(values.get("iname")[0]);
		toSave.setSurname(values.get("surname")[0]);
		toSave.setEmail(values.get("email1")[0]);
		String email2 = values.get("email2")[0];
		
		//validation
		
		if (values.get("passwd1")[0].length() < 5)
			return badRequest(register.render("Password should be at least 5 characters long.",
					toSave.getName(), toSave.getSurname(), toSave.getEmail(), email2));
		
		if ( ! values.get("passwd1")[0].equals(values.get("passwd2")[0]))
		{
			Logger.info("diff passwd: " + values.get("passwd1")[0] + " " + values.get("passwd2")[0]);
			return badRequest(register.render("Passwords do not match.",
					toSave.getName(), toSave.getSurname(), toSave.getEmail(), email2));
		}
		
		List <Member> list = Member.find.where().eq("email", values.get("email1")[0]).findList();
		if ( ! list.isEmpty())
		{
			for (Member m : list) Logger.info("E-MAIL " + m.getEmail() + " IN USE.");
			return badRequest(register.render("E-mail already in use.",
					toSave.getName(), toSave.getSurname(), toSave.getEmail(), email2));
		}
		
		// end of validation
		
		try
		{
			toSave.setPassword(values.get("passwd1")[0]);
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
			return badRequest(register.render("Failed to register password. (BUG)",
					toSave.getName(), toSave.getSurname(), toSave.getEmail(), email2));
		}
		toSave.save();
		Logger.info("Succesfully added " + toSave.getName() + " " + toSave.getSurname() + ".");
		return redirect(controllers.routes.MainController.home());
	}
}
