package controllers;

import models.Employee;
import models.Facility;
import models.Receptionist;
import models.Trainer;

public class EmployeeController
{
	void addFacility(Employee manager)
	{
		if (manager.isManager())
		{
			Facility facility = new Facility();
			//TODO
			facility.save();
		}
	}
	
	void removeFacility(Employee manager, Facility facility)
	{
		if (manager.isManager())
		{
			//TODO
			facility.delete();
			facility = null;
		}
	}
	
	void addTrainer(Employee manager)
	{
		if (manager.isManager())
		{
			Trainer trainer = new Trainer();
			//TODO
			trainer.save();
		}
	}
	
	void addReceptionist(Employee manager)
	{
		if (manager.isManager())
		{
			Receptionist receptionist = new Receptionist();
			//TODO
			receptionist.save();
		}
	}
}
