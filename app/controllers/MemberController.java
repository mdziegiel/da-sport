package controllers;

import models.Competition;
import models.Exercise;
import models.Group;
import models.Member;
import models.Participation;

public class MemberController
{
	void unregister(Member member)
	{
		//TODO
		member.delete();
		member = null;
	}
	
	void addExercise()
	{
		Exercise exercise = new Exercise();
		//TODO reserve spots wherever it matters, reserve a trainer's time etc. (?)
		exercise.save();
	}
	
	void removeExercise(Exercise exercise)
	{
		//TODO
		exercise.delete();
		exercise = null;
	}
	
	void addGroup(Member member)
	{
		Group group = new Group();
		group.getMembers().add(member);
		group.setOwner(member);
		group.save();
	}
	
	void disbandGroup(Member whoDeletes, Group group)
	{
		if (whoDeletes == group.getOwner())
		{
			//TODO
			group.delete();
			group = null;
		}
	}
	
	void moveGroupOwnership(Group group, Member whoMoves, Member newOwner)
	{
		if (whoMoves == group.getOwner()) group.setOwner(newOwner);
	}
	
	void addFriend(Member friend)
	{
		// TODO ?
	}
	
	void removeFriend(Member friend)
	{
		// TODO ?
	}
	
	void applyForCompetition(Member member, Competition competition)
	{
		Participation p = new Participation();
		p.setCompetition(competition);
		p.setParticipant(member);
		p.setPoints(0);
		competition.getParticipants().add(p);
		p.save();
		competition.save();
	}
	
	void resignFromCompetition(Member member, Competition competition)
	{
		competition.getParticipants().remove(0); // TODO "participation where member == member"
	}
}
