package controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import models.Administrator;
import models.Event;
import models.Member;
import models.Receptionist;
import models.Trainer;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.home;

public class Authentication extends Controller
{
	public static Result login()
	{
		List<Event> news = Event.find.orderBy("endTime").findList();
		final Map<String, String[]> values = request().body().asFormUrlEncoded();
		
		List<Member> members = Member.find.where().eq("email", values.get("email")[0]).findList();
		if (! members.isEmpty())
		{
			try
			{
				if (! members.get(0).passwordCorrect(values.get("passwd")[0])) return badRequest(home.render("No user with e-mail \"" + values.get("email")[0] + "\", or password is wrong.", news));
			}
			catch (NoSuchAlgorithmException e)
			{
				e.printStackTrace();
				return badRequest(home.render("Failed to retrieve password from DB. (BUG)", news));
			}
			
			session().clear();
			session("userEmail", values.get("email")[0]);
			session("userType", "Member");
			session("userName", members.get(0).getName());
			return redirect(controllers.routes.MainController.home());
		}
		
		List<Trainer> trainers = Trainer.find.where().eq("email", values.get("email")[0]).findList();
		if (! trainers.isEmpty())
		{
			try
			{
				if (! trainers.get(0).passwordCorrect(values.get("passwd")[0])) return badRequest(home.render("No user with e-mail \"" + values.get("email")[0] + "\", or password is wrong.", news));
			}
			catch (NoSuchAlgorithmException e)
			{
				e.printStackTrace();
				return badRequest(home.render("Failed to retrieve password from DB. (BUG)", news));
			}
			
			session().clear();
			session("userEmail", values.get("email")[0]);
			session("userType", "Trainer");
			session("userName", trainers.get(0).getName());
			return redirect(controllers.routes.MainController.home());
		}
		
		List<Receptionist> recs = Receptionist.find.where().eq("email", values.get("email")[0]).findList();
		if (! recs.isEmpty())
		{
			try
			{
				if (! recs.get(0).passwordCorrect(values.get("passwd")[0])) return badRequest(home.render("No user with e-mail \"" + values.get("email")[0] + "\", or password is wrong.", news));
			}
			catch (NoSuchAlgorithmException e)
			{
				e.printStackTrace();
				return badRequest(home.render("Failed to retrieve password from DB. (BUG)", news));
			}
			
			session().clear();
			session("userEmail", values.get("email")[0]);
			session("userType", "Receptionist");
			session("userName", recs.get(0).getName());
			return redirect(controllers.routes.MainController.home());
		}
		
		List<Administrator> admins = Administrator.find.where().eq("email", values.get("email")[0]).findList();
		if (! admins.isEmpty())
		{
			try
			{
				if (! admins.get(0).passwordCorrect(values.get("passwd")[0])) return badRequest(home.render("No user with e-mail \"" + values.get("email")[0] + "\", or password is wrong.", news));
			}
			catch (NoSuchAlgorithmException e)
			{
				e.printStackTrace();
				return badRequest(home.render("Failed to retrieve password from DB. (BUG)", news));
			}
			
			session().clear();
			session("userEmail", values.get("email")[0]);
			session("userType", "Administrator");
			session("userName", admins.get(0).getName());
			return redirect(controllers.routes.MainController.home());
		}
		
		return badRequest(home.render("No user with e-mail \"" + values.get("email")[0] + "\", or password is wrong.", news));
	}
	
	public static Result logout()
	{
		session().clear();
		return redirect(controllers.routes.MainController.home());
	}
}