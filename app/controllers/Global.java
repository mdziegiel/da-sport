package controllers;

import play.GlobalSettings;
import play.libs.F.Promise;
import play.mvc.Result;
import play.mvc.Results;
import play.mvc.Http.RequestHeader;
import views.html.*;

public class Global extends GlobalSettings
{
	@Override
	public Promise<Result> onHandlerNotFound(RequestHeader request)
	{
		return Promise.<Result>pure(Results.notFound(error.render("Error 404: Not Found...", "Sorry?")));
	}
	
	@Override
	public Promise<Result> onError(RequestHeader request, Throwable throwable)
	{
		return Promise.<Result>pure(Results.notFound(error.render("Error 500: Internal Server Error...", "Is anybody a doctor? ...I mean programmer?")));
	}
}