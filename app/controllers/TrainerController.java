package controllers;

import models.Competition;
import models.Exercise;
import models.Group;

public class TrainerController
{
	void addCompetition()
	{
		Competition competition = new Competition();
		//TODO
		competition.save();
	}
	
	void cancelCompetition(Competition competition)
	{
		//TODO
		competition.delete();
		competition = null;
	}
	
	void addGroup()
	{
		Group group = new Group();
		//TODO
		group.save();
	}
	
	void disbandGroup(Group group)
	{
		//TODO
		group.delete();
		group = null;
	}
	
	void addExercise()
	{
		Exercise exercise = new Exercise();
		//TODO reserve spots wherever it matters, reserve a trainer's time etc. (?)
		exercise.save();
	}
	
	void removeExercise(Exercise exercise)
	{
		//TODO
		exercise.delete();
		exercise = null;
	}
}
