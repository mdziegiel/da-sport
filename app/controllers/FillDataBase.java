package controllers;

import java.security.NoSuchAlgorithmException;
import java.util.List;
import play.mvc.Controller;
import play.mvc.Result;
import models.*;

public class FillDataBase extends Controller
{
	public static Result fillDB()
	{
		//ADMIN
		Administrator admin = new Administrator();
		admin.setDescription("Admin");
		admin.setEmail("admin@sportart.pl");
		admin.setName("Adam");
		admin.setSurname("Brodaty");
		try
		{
			admin.setPassword("admin");
		}
		catch (NoSuchAlgorithmException e)
		{
			//TODO
		}
		admin.save();
		
		//CLUBS
		Club c1 = new Club();
		c1.setCity("Lublin");
		c1.setStreet("Zana");
		c1.setBuilding("39a");
		c1.setDescription("The club, you know, there, that... yeah, that one.");
		c1.save();
		
		Club c2 = new Club();
		c2.setCity("Dublin");
		c2.setStreet("Grove Park");
		c2.setBuilding("14/8");
		c2.setDescription("The other club.");
		c2.save();
		
		//DISCIPLINES
		Discipline d1 = new Discipline();
		d1.setName("Throwing stones");
		Discipline d2 = new Discipline();
		d2.setName("Sitting");
		Discipline d3 = new Discipline();
		d3.setName("Sleeping");
		Discipline d4 = new Discipline();
		d4.setName("Standing upside down");
		Discipline d5 = new Discipline();
		d5.setName("Eating");
		
		d1.save();
		d2.save();
		d3.save();
		d4.save();
		d5.save();
		
		//EVENTS
		Event e1 = new Event();
		e1.setTitle("New classes!");
		e1.setDescription("Now you can try our yoga class.");
		e1.setStartTime(null);
		e1.setEndTime(null);
		e1.save();
		
		Event e2 = new Event();
		e2.setTitle("Database in action.");
		e2.setDescription("... yes.");
		e2.setStartTime(null);
		e2.setEndTime(null);
		e2.save();
		
		//TRAINERS
		List<Discipline> disciplines = Discipline.find.all();
		Trainer t1 = new Trainer();
		t1.setName("Telesfor");
		t1.setSurname("Podłokietnik");
		t1.setSpecialties(disciplines);
		t1.setDescription("I love cats, they're delicious.");
		t1.setClub(c1);
		t1.setEmail("tele@sportart.pl");
		t1.setManager(true);
		try
		{
			t1.setPassword("trainer");
		}
		catch (NoSuchAlgorithmException e)
		{
			//TODO
		}
		t1.save();
		
		Trainer t2 = new Trainer();
		t2.setName("Janusz");
		t2.setSurname("Komar");
		t2.setSpecialties(disciplines);
		t2.setDescription("I hate cats.");
		t2.setClub(c1);
		t2.setEmail("janusz@sportart.pl");
		t2.setManager(false);
		try
		{
			t2.setPassword("trainer");
		}
		catch (NoSuchAlgorithmException e)
		{
			//TODO
		}
		t2.save();
		
		Trainer t3 = new Trainer();
		t3.setName("Genowefa");
		t3.setSurname("Serdeczna");
		t3.setSpecialties(disciplines);
		t3.setDescription("I love dogs.");
		t3.setClub(c2);
		t3.setEmail("genius123@sportart.pl");
		t3.setManager(true);
		try
		{
			t3.setPassword("trainer");
		}
		catch (NoSuchAlgorithmException e)
		{
			//TODO
		}
		t3.save();
		
		return redirect(controllers.routes.MainController.home());
	}
}
