package controllers;

import java.util.ArrayList;
import java.util.List;

import models.Administrator;
import models.Club;
import models.Discipline;
import models.Event;
import models.Member;
import models.Receptionist;
import models.Trainer;
import models.User;
import play.Logger;
import play.mvc.*;
import play.twirl.api.Html;
import views.html.*;

public class MainController extends Controller
{
	public static Result home()
	{
		String message = "Some dream about goals... now you don't have to!";
		List<Event> news = Event.find.orderBy("endTime").findList();
		
		if (session().get("userEmail") != null)
		{
			message = "Welcome, " + session().get("userName") + "!";
		}
		return ok(home.render(message, news));
	}
	
	public static Result about()
	{
		return ok(about.render());
	}
	
	public static Result clubs()
	{
		List<Club> c = Club.find.all();
		return ok(clubs.render(c));
	}
	
	public static Result club(int id)
	{
		Club c = Club.find.byId(id);
		return ok(club.render(c));
	}
	
	public static Result competitions()
	{
		return TODO;
	}
	
	public static Result event(int id)
	{
		Event e = Event.find.byId(id);
		return ok(event.render(e));
	}
	
	public static Result facilities()
	{
		return TODO;
	}
	
	public static Result schedule()
	{
		return ok(schedule.render());
	}
	
	public static Result trainers()
	{
		List<Trainer> t = Trainer.find.all();
		return ok(trainers.render(t));
	}
	
	public static Result trainer(int id)
	{
		Trainer t = Trainer.find.byId(id);
		return ok(trainer.render(t));
	}
}
