# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table administrators (
  id                        integer not null,
  name                      varchar(255),
  surname                   varchar(255),
  email                     varchar(255),
  description               varchar(255),
  passhash                  varchar(255),
  logged_in                 boolean,
  present                   boolean,
  constraint pk_administrators primary key (id))
;

create table clubs (
  id                        integer not null,
  city                      varchar(255),
  street                    varchar(255),
  building                  varchar(255),
  description               varchar(255),
  constraint pk_clubs primary key (id))
;

create table competitions (
  id                        integer not null,
  start_time                timestamp,
  end_time                  timestamp,
  title                     varchar(255),
  description               varchar(255),
  trainer_id                integer,
  rewards                   varchar(255),
  points                    integer,
  constraint pk_competitions primary key (id))
;

create table disciplines (
  id                        integer not null,
  name                      varchar(255),
  constraint pk_disciplines primary key (id))
;

create table discounts (
  id                        integer not null,
  start_time                timestamp,
  end_time                  timestamp,
  title                     varchar(255),
  description               varchar(255),
  receptionist_id           integer,
  points                    integer,
  constraint pk_discounts primary key (id))
;

create table events (
  id                        integer not null,
  start_time                timestamp,
  end_time                  timestamp,
  title                     varchar(255),
  description               varchar(255),
  constraint pk_events primary key (id))
;

create table exercises (
  id                        integer not null,
  start_time                timestamp,
  end_time                  timestamp,
  title                     varchar(255),
  description               varchar(255),
  facility_id               integer,
  member_id                 integer,
  group_id                  integer,
  trainer_id                integer,
  competition_id            integer,
  constraint pk_exercises primary key (id))
;

create table facilities (
  id                        integer not null,
  club_id                   integer,
  description               varchar(255),
  caretaker_id              integer,
  slots                     integer,
  price                     integer,
  discount_id               integer,
  constraint pk_facilities primary key (id))
;

create table groups (
  id                        integer not null,
  name                      varchar(255),
  description               varchar(255),
  owner_id                  integer,
  constraint pk_groups primary key (id))
;

create table members (
  id                        integer not null,
  name                      varchar(255),
  surname                   varchar(255),
  email                     varchar(255),
  description               varchar(255),
  passhash                  varchar(255),
  logged_in                 boolean,
  present                   boolean,
  points                    integer not null,
  constraint pk_members primary key (id))
;

create table participations (
  id                        integer not null,
  competition_id            integer,
  participant_id            integer,
  points                    integer not null,
  constraint pk_participations primary key (id))
;

create table receptionists (
  id                        integer not null,
  name                      varchar(255),
  surname                   varchar(255),
  email                     varchar(255),
  description               varchar(255),
  passhash                  varchar(255),
  logged_in                 boolean,
  present                   boolean,
  manager                   boolean not null,
  club_id                   integer,
  constraint pk_receptionists primary key (id))
;

create table trainers (
  id                        integer not null,
  name                      varchar(255),
  surname                   varchar(255),
  email                     varchar(255),
  description               varchar(255),
  passhash                  varchar(255),
  logged_in                 boolean,
  present                   boolean,
  manager                   boolean not null,
  club_id                   integer,
  constraint pk_trainers primary key (id))
;


create table facilities_disciplines (
  facilities_id                  integer not null,
  disciplines_id                 integer not null,
  constraint pk_facilities_disciplines primary key (facilities_id, disciplines_id))
;

create table groups_members (
  groups_id                      integer not null,
  members_id                     integer not null,
  constraint pk_groups_members primary key (groups_id, members_id))
;

create table trainers_disciplines (
  trainers_id                    integer not null,
  disciplines_id                 integer not null,
  constraint pk_trainers_disciplines primary key (trainers_id, disciplines_id))
;
create sequence administrators_seq;

create sequence clubs_seq;

create sequence competitions_seq;

create sequence disciplines_seq;

create sequence discounts_seq;

create sequence events_seq;

create sequence exercises_seq;

create sequence facilities_seq;

create sequence groups_seq;

create sequence members_seq;

create sequence participations_seq;

create sequence receptionists_seq;

create sequence trainers_seq;

alter table competitions add constraint fk_competitions_trainer_1 foreign key (trainer_id) references trainers (id) on delete restrict on update restrict;
create index ix_competitions_trainer_1 on competitions (trainer_id);
alter table discounts add constraint fk_discounts_receptionist_2 foreign key (receptionist_id) references receptionists (id) on delete restrict on update restrict;
create index ix_discounts_receptionist_2 on discounts (receptionist_id);
alter table exercises add constraint fk_exercises_facility_3 foreign key (facility_id) references facilities (id) on delete restrict on update restrict;
create index ix_exercises_facility_3 on exercises (facility_id);
alter table exercises add constraint fk_exercises_member_4 foreign key (member_id) references members (id) on delete restrict on update restrict;
create index ix_exercises_member_4 on exercises (member_id);
alter table exercises add constraint fk_exercises_group_5 foreign key (group_id) references groups (id) on delete restrict on update restrict;
create index ix_exercises_group_5 on exercises (group_id);
alter table exercises add constraint fk_exercises_trainer_6 foreign key (trainer_id) references trainers (id) on delete restrict on update restrict;
create index ix_exercises_trainer_6 on exercises (trainer_id);
alter table exercises add constraint fk_exercises_competition_7 foreign key (competition_id) references competitions (id) on delete restrict on update restrict;
create index ix_exercises_competition_7 on exercises (competition_id);
alter table facilities add constraint fk_facilities_club_8 foreign key (club_id) references clubs (id) on delete restrict on update restrict;
create index ix_facilities_club_8 on facilities (club_id);
alter table facilities add constraint fk_facilities_caretaker_9 foreign key (caretaker_id) references trainers (id) on delete restrict on update restrict;
create index ix_facilities_caretaker_9 on facilities (caretaker_id);
alter table facilities add constraint fk_facilities_discount_10 foreign key (discount_id) references discounts (id) on delete restrict on update restrict;
create index ix_facilities_discount_10 on facilities (discount_id);
alter table groups add constraint fk_groups_owner_11 foreign key (owner_id) references members (id) on delete restrict on update restrict;
create index ix_groups_owner_11 on groups (owner_id);
alter table participations add constraint fk_participations_competition_12 foreign key (competition_id) references competitions (id) on delete restrict on update restrict;
create index ix_participations_competition_12 on participations (competition_id);
alter table participations add constraint fk_participations_participant_13 foreign key (participant_id) references members (id) on delete restrict on update restrict;
create index ix_participations_participant_13 on participations (participant_id);
alter table receptionists add constraint fk_receptionists_club_14 foreign key (club_id) references clubs (id) on delete restrict on update restrict;
create index ix_receptionists_club_14 on receptionists (club_id);
alter table trainers add constraint fk_trainers_club_15 foreign key (club_id) references clubs (id) on delete restrict on update restrict;
create index ix_trainers_club_15 on trainers (club_id);



alter table facilities_disciplines add constraint fk_facilities_disciplines_fac_01 foreign key (facilities_id) references facilities (id) on delete restrict on update restrict;

alter table facilities_disciplines add constraint fk_facilities_disciplines_dis_02 foreign key (disciplines_id) references disciplines (id) on delete restrict on update restrict;

alter table groups_members add constraint fk_groups_members_groups_01 foreign key (groups_id) references groups (id) on delete restrict on update restrict;

alter table groups_members add constraint fk_groups_members_members_02 foreign key (members_id) references members (id) on delete restrict on update restrict;

alter table trainers_disciplines add constraint fk_trainers_disciplines_train_01 foreign key (trainers_id) references trainers (id) on delete restrict on update restrict;

alter table trainers_disciplines add constraint fk_trainers_disciplines_disci_02 foreign key (disciplines_id) references disciplines (id) on delete restrict on update restrict;

# --- !Downs

SET REFERENTIAL_INTEGRITY FALSE;

drop table if exists administrators;

drop table if exists clubs;

drop table if exists competitions;

drop table if exists disciplines;

drop table if exists trainers_disciplines;

drop table if exists facilities_disciplines;

drop table if exists discounts;

drop table if exists events;

drop table if exists exercises;

drop table if exists facilities;

drop table if exists groups;

drop table if exists groups_members;

drop table if exists members;

drop table if exists participations;

drop table if exists receptionists;

drop table if exists trainers;

SET REFERENTIAL_INTEGRITY TRUE;

drop sequence if exists administrators_seq;

drop sequence if exists clubs_seq;

drop sequence if exists competitions_seq;

drop sequence if exists disciplines_seq;

drop sequence if exists discounts_seq;

drop sequence if exists events_seq;

drop sequence if exists exercises_seq;

drop sequence if exists facilities_seq;

drop sequence if exists groups_seq;

drop sequence if exists members_seq;

drop sequence if exists participations_seq;

drop sequence if exists receptionists_seq;

drop sequence if exists trainers_seq;

